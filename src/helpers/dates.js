/* eslint-disable max-len */
// this can be used for time/date related functions;

// converts epoch to OS locale time
const convertEpochToHourMinute = (timestamp) => {
  const inputTime = new Date(timestamp * 1000);
  return `${inputTime.getHours()}:${inputTime.getMinutes()}`;
};

const convertEpochToDayOfTheWeek = (timestamp) => {
  const inputTime = new Date(timestamp * 1000);
  const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  return days[inputTime.getDay()];
};
const convertEpochToHour = (timestamp) => {
  const inputTime = new Date(timestamp * 1000);
  return `${inputTime.getHours()}`;
};

// gets epoch time stamps and returns the time for specified timezone in 00:00 format
const getFormattedTimeForTimeZone = (timestamp, timezoneString) => {
  const time = new Date(timestamp * 1000); // to get result in miliseconds
  return time.toLocaleString('en-GB', { timeZone: timezoneString, hour: '2-digit', minute: '2-digit' });
};

export const dates = {
  convertEpochToHourMinute,
  convertEpochToDayOfTheWeek,
  getFormattedTimeForTimeZone,
  convertEpochToHour,
};
