/* eslint-disable no-trailing-spaces */
export const config = { 
  keys: {
    weatherApiKey: '383f8caa6efc8b0283c110ebfa9c3872',
    forecastApiKey: '3230bd18ba4c19157c518f41f82d237f',
    hourlyApiKey: 'e0ff52a5121da4f9bff9085afacf5e63',
  },
  defaults: {
    city: 'SOFIA',
  },
  cities: [
    'Plovdiv',
    'Varna',
    'Burgas',
    'Moscow',
    'Berlin',
    'London',
  ],
  timeZone: '',
};
