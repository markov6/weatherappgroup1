/* eslint-disable max-len */
import { config } from './config.js';

const baseUrl = 'http://api.openweathermap.org/data/2.5/';

const getWeatherByName = (cityName) => {
  return $.get(`${baseUrl}weather?q=${cityName}&appid=${config.keys.weatherApiKey}&units=metric`);
};

const getWeatherHourly = (cityName) => {
  return $.get(`${baseUrl}forecast?q=${cityName}&appid=${config.keys.hourlyApiKey}&units=metric`);
};

const getForecastByName = (cityName) => {
  return $.get(`${baseUrl}forecast?q=${cityName}&appid=${config.keys.forecastApiKey}&units=metric`);
};

// for now we use this API just to get the timezone name for given city
const getTimezone = (cityName) => {
  return $.get(`https://api.apixu.com/v1/forecast.json?key=3b3c655fc0cc4688ae5135717192103&q=${cityName}`);
};


export const database = {
  getWeatherByName,
  getWeatherHourly,
  getForecastByName,
  getTimezone,
};
