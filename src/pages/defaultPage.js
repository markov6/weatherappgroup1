/* eslint-disable max-len */
import { config } from '../helpers/config.js';
import { database } from '../helpers/database.js';
import { dates } from '../helpers/dates.js';

// we create array with the two API calls, then we use Promise.all which returns array with the resolved promises
// and only .then we add the dynamic data, the showData function is then called in main.js

const showData = (city) => {
  const timeZoneRender = [database.getTimezone(city), database.getWeatherByName(city)];
  Promise.all(timeZoneRender)
      .then(displayBasicData)
      .catch(error => { alert(error.responseJSON.message)});
};

const displayBasicData = (data) => {
  const timeZone = data[0].location.tz_id;
  config.timeZone = timeZone;
  // .views-container is what all can use, it's static in html and styles
  $('.views-container').html(`<div id='weather-now-flex-container'></div>`);
  // adding 3 additional containers for the now views
  $('#weather-now-flex-container').html(`<div id='city-container' class='now-container'></div>
    <div id='now-weather-temp' class='now-container'></div>
    <div id='now-weather-details' class='now-container'></div>`);
  // filling up container 1
  $('#city-container').html(`<h1>${data[1].name}</h1>
    <img src='./src/assets/images/${data[1].weather[0].icon}.png'>
    <h4>${data[1].weather[0].description.toUpperCase()}</h4>`);
  // container 2 is for temp only
  $('#now-weather-temp').html(`<h1>${Math.round(data[1].main.temp)}°C</h1>`);
  // container 3 is for weather now details
  $('#now-weather-details').html(`<p>Atm. pressure: ${data[1].main.pressure} hPa</p>
    <p>Humidity: ${data[1].main.humidity} %</p>
    <p>Wind: ${data[1].wind.speed} meter/sec</p>
    <p>Cloudiness: ${data[1].clouds.all} %</p>
    <p>Sunrise: ${dates.getFormattedTimeForTimeZone(data[1].sys.sunrise, timeZone)}</p>
    <p>Sunset: ${dates.getFormattedTimeForTimeZone(data[1].sys.sunset, timeZone)}</p>`);
};

export const defaultPage = {
  showData,
};
