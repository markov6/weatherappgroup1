/* eslint-disable max-len */
import { database } from '../helpers/database.js';
import { dates } from '../helpers/dates.js';
import { config } from '../helpers/config.js'

const displayHourData = (data) => {
  $('.views-container').html(`<h2 id="cityName">${data.city.name}</h2> <div id='weather-24h-flex-container' ></div>`);
  $('#cityName').css('text-align', 'center');
  $('#weather-24h-flex-container').html(`<ul id='hour-list' class='list'></ul>
    <ul id= 'second-hour-list' class='list'></ul>`);
  $('#hour-list').html(`<il> ${dates.getFormattedTimeForTimeZone(data.list[0].dt, config.timeZone)}
  ${Math.floor(data.list[0].main.temp)}°C 
  <img src='./src/assets/images/${data.list[0].weather[0].icon}.png' style="height: 40px;
  width: 40px;">
  <h1>${data.list[0].weather[0].description}</h1></il>
  <il> ${dates.getFormattedTimeForTimeZone(data.list[1].dt, config.timeZone )}
  ${Math.floor(data.list[1].main.temp)}°C 
  <img src='./src/assets/images/${data.list[1].weather[0].icon}.png' style="height: 40px;
  width: 40px;">
  <h1>${data.list[1].weather[0].description}</h1></il>
  <il> ${dates.getFormattedTimeForTimeZone(data.list[2].dt, config.timeZone)}
  ${Math.floor(data.list[2].main.temp)}°C 
  <img src='./src/assets/images/${data.list[2].weather[0].icon}.png' style="height: 40px;
  width: 40px;">
  <h1>${data.list[2].weather[0].description}</h1></il>
  <il> ${dates.getFormattedTimeForTimeZone(data.list[3].dt, config.timeZone)}
  ${Math.floor(data.list[3].main.temp)}°C 
  <img src='./src/assets/images/${data.list[3].weather[0].icon}.png' style="height: 40px;
  width: 40px;">
  <h1>${data.list[3].weather[0].description}</h1>
  </il>`);
  $('#second-hour-list').html(`<il> ${dates.getFormattedTimeForTimeZone(data.list[4].dt, config.timeZone)}
  ${Math.floor(data.list[4].main.temp)}°C 
  <img src='./src/assets/images/${data.list[4].weather[0].icon}.png' style="height: 40px;
  width: 40px;">
  <h1>${data.list[4].weather[0].description}</h1></il>
  <il> ${dates.getFormattedTimeForTimeZone(data.list[5].dt, config.timeZone)}
  ${Math.floor(data.list[5].main.temp)}°C 
  <img src='./src/assets/images/${data.list[5].weather[0].icon}.png' style="height: 40px;
  width: 40px;"><h1>${data.list[5].weather[0].description}</h1></il>
  <il> ${dates.getFormattedTimeForTimeZone(data.list[6].dt, config.timeZone)}
  ${Math.floor(data.list[6].main.temp)}°C 
  <img src='./src/assets/images/${data.list[6].weather[0].icon}.png' style="height: 40px;
  width: 40px;"><h1>${data.list[6].weather[0].description}</h1></il>
  <il> ${dates.getFormattedTimeForTimeZone(data.list[7].dt, config.timeZone)} 
  ${Math.floor(data.list[7].main.temp)}°C 
  <img src='./src/assets/images/${data.list[7].weather[0].icon}.png' style="height: 40px;
  width: 40px;"><h1>${data.list[7].weather[0].description}</h1></il>`);
};

console.log(config.timeZone)
const render = (city) => {
  database.getWeatherHourly(city).then(displayHourData);
};
export const dailyPage = {
  render,
};
// ${dates.convertEpochToHour(data.list[0].dt)}
