
import { database } from '../helpers/database.js';

const render = (arr) => {
  arr.forEach((element, i) => {
    database.getWeatherByName(element)
        .then((data) => { 
          $(`#city${i}`).html(`<h1>${element}</h1>`);
          $(`#city${i}-temp`).html(`${Math.round(data.main.temp)}°C`)
          $(`#city${i}-icon`).html(`<img src='./src/assets/images/${data.weather[0].icon}.png'>`);
          $(`#city${i}-description`).html(`${data.weather[0].description.toUpperCase()}`);
        });
});
};

export const table = {
  render,
};
