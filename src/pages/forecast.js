import { database } from '../helpers/database.js';
import { dates } from '../helpers/dates.js';
import { config } from '../helpers/config.js';

const displayBasicData = (data) => {

  const arr = (data.list).reduce((acc, element, index, array) => {
    const formattedTimeForTimeZone = dates.getFormattedTimeForTimeZone(element.dt, config.timeZone)
    if ((formattedTimeForTimeZone === '14:00' || formattedTimeForTimeZone === '13:00' || formattedTimeForTimeZone === '12:00' )) {
      acc.push(element);
    }
    return acc;
  }, []);
  $('.views-container').html(`<h2 id="cityName">${data.city.name}</h2> <div id='weather-5-days-flex-container'></div>`);
  $('#cityName').css('text-align', 'center');

  $('#weather-5-days-flex-container').html(`<div id='day1' class='city-container-five-days-container'></div>
  <div id='day2' class='city-container-five-days-container'></div>
  <div id='day3' class='city-container-five-days-container'></div>
  <div id='day4' class='city-container-five-days-container'></div>
  <div id='day5' class='city-container-five-days-container'></div>`);

    

  $('#day1').html(`<h1>${dates.convertEpochToDayOfTheWeek(arr[0].dt)}</h1>
    <h4 id="five-days-weather-temp">${Math.round(arr[0].main.temp)}°C</h4>
    <img src='./src/assets/images/${arr[0].weather[0].icon}.png' style="height: 105px;"
    width: 105px;>
    <h3>${arr[0].weather[0].description.toUpperCase()}</h3>`);
    $('#day2').html(`<h1>${dates.convertEpochToDayOfTheWeek(arr[1].dt)}</h1>
    <h4 id="five-days-weather-temp">${Math.round(arr[1].main.temp)}°C</h4>
    <img src='./src/assets/images/${arr[1].weather[0].icon}.png' style="height: 105px;"
    width: 105px;>
    <h3>${arr[1].weather[0].description.toUpperCase()}</h3>`);
    $('#day3').html(`<h1>${dates.convertEpochToDayOfTheWeek(arr[2].dt)}</h1>
    <h4 id="five-days-weather-temp">${Math.round(arr[2].main.temp)}°C</h4>
    <img src='./src/assets/images/${arr[2].weather[0].icon}.png' style="height: 105px;"
    width: 105px;>
    <h3>${arr[2].weather[0].description.toUpperCase()}</h3>`);
    $('#day4').html(`<h1>${dates.convertEpochToDayOfTheWeek(arr[3].dt)}</h1>
    <h4 id="five-days-weather-temp">${Math.round(arr[3].main.temp)}°C</h4>
    <img src='./src/assets/images/${arr[3].weather[0].icon}.png' style="height: 105px;"
    width: 105px;>
    <h3>${arr[3].weather[0].description.toUpperCase()}</h3>`);
    $('#day5').html(`<h1>${dates.convertEpochToDayOfTheWeek(arr[4].dt)}</h1>
    <img src='./src/assets/images/${arr[0].weather[0].icon}.png' style="height: 105px;"
    width: 105px;>
    <h4>${arr[0].weather[0].description.toUpperCase()}</h4>`);
  $('#day2').html(`<h1>${dates.convertEpochToDayOfTheWeek(arr[1].dt)}</h1>
    <h4 id="five-days-weather-temp">${Math.round(arr[1].main.temp)}°C</h4>
    <img src='./src/assets/images/${arr[1].weather[0].icon}.png' style="height: 105px;"
    width: 105px;>
    <h4>${arr[1].weather[0].description.toUpperCase()}</h4>`);
  $('#day3').html(`<h1>${dates.convertEpochToDayOfTheWeek(arr[2].dt)}</h1>
    <h4 id="five-days-weather-temp">${Math.round(arr[2].main.temp)}°C</h4>
    <img src='./src/assets/images/${arr[2].weather[0].icon}.png' style="height: 105px;"
    width: 105px;>
    <h4>${arr[2].weather[0].description.toUpperCase()}</h4>`);
  $('#day4').html(`<h1>${dates.convertEpochToDayOfTheWeek(arr[3].dt)}</h1>
    <h4 id="five-days-weather-temp">${Math.round(arr[3].main.temp)}°C</h4>
    <img src='./src/assets/images/${arr[3].weather[0].icon}.png' style="height: 105px;"
    width: 105px;>
    <h4>${arr[3].weather[0].description.toUpperCase()}</h4>`);
  $('#day5').html(`<h1>${dates.convertEpochToDayOfTheWeek(arr[4].dt)}</h1>
    <h4 id="five-days-weather-temp">${Math.round(arr[4].main.temp)}°C</h4>
    <img src='./src/assets/images/${arr[4].weather[0].icon}.png' style="height: 105px;"
    width: 105px;>
    <h3>${arr[4].weather[0].description.toUpperCase()}</h3>`);

    
};

const render = (city) => {
  database.getForecastByName(city).then(displayBasicData);
};

export const foreCast = {
  render,
};
