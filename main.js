/* eslint-disable max-len */
// int this file we can define the events for the different buttons
import { config } from './src/helpers/config.js';
import { defaultPage } from './src/pages/defaultPage.js';
import { foreCast } from './src/pages/forecast.js';
import { dailyPage } from './src/pages/dailyPage.js';
import  { table } from './src/pages/table.js';

let currentCity; // this variable should define what we show in different views

// first we can check in browser local storage if we have value for favouriteCity key(set with #default-btn click)
if (window.localStorage.getItem('favouriteCity') !== null) {
  currentCity = window.localStorage.getItem('favouriteCity');
// here we can implement another condition for geolocation
} else {// this is last resort fallback to harcoded city value, so we can always render something on start;
  currentCity = config.defaults.city;
}
// this can be the fallback value if nothing else is selected eg. no favourite city, no geolocation

// this is the event handler for search button click
$('#search-city-button').on('click', () => {
  // change currentCity to input field value
  const inputString = $('.search-input').val();
  if (inputString !== '') {
    currentCity = $('.search-input').val();
    defaultPage.showData(currentCity);
  }
  // this sets a class to higlight the favouriute button
  if (currentCity === window.localStorage.getItem('favouriteCity')) {
    $('.fa.fa-star-o').addClass('favourite-highlight');
  } else {
    $('.fa.fa-star-o').removeClass('favourite-highlight');
  }
  $('#btn-weather-24').css('background-color', 'transparent');
  $('#btn-weather-5').css('background-color', 'transparent');
  $('#btn-weather-now').css('background-color', 'rgba(255, 255, 255, 0.5)');  
});

// this is again for seach input, but listening for Enter key press
$('#input-field').keypress((event) => {
  const keycode = (event.keyCode ? event.keyCode : event.which);
  if (keycode === 13) {
    $('#search-city-button').trigger('click');
  }
});

$('#default-btn.btn').on('click', () => {
  // we can always get this from currentCity variable, as long as we have valid string there.
  window.localStorage.setItem('favouriteCity', currentCity);
  $('.fa.fa-star-o').addClass('favourite-highlight')
});

// this sets the class for favourite highlight when we show favourite city by default;
if (currentCity === window.localStorage.getItem('favouriteCity')) {
  $('.fa.fa-star-o').addClass('favourite-highlight');
}
$('#btn-weather-now').on('click', () => {
  $('#btn-weather-24').css('background-color', 'transparent');
  $('#btn-weather-5').css('background-color', 'transparent');
  $('#btn-weather-now').css('background-color', 'rgba(255, 255, 255, 0.5)');
  defaultPage.showData(currentCity);
  table.render(config.cities);
});

$('#btn-weather-24').on('click', () => {
  $('#btn-weather-now').css('background-color', 'transparent');
  $('#btn-weather-5').css('background-color', 'transparent');
  $('#btn-weather-24').css('background-color', 'rgba(255, 255, 255, 0.5)');
  dailyPage.render(currentCity);
});

$('#btn-weather-5').on('click', () => {
  $('#btn-weather-now').css('background-color', 'transparent');
  $('#btn-weather-24').css('background-color', 'transparent');
  $('#btn-weather-5').css('background-color', 'rgba(255, 255, 255, 0.5)');
  foreCast.render(currentCity);
});
defaultPage.showData(currentCity);
table.render(config.cities);